package com.sharif.mk.weatherme;

public class WeatherModel  {

    private String dayOfWeek;
    private String stateOfDay;
    private String HighTemp;
    private String  lowTemp;

    public WeatherModel(String dayOfWeek, String stateOfDay, String highTemp, String lowTemp) {
        this.dayOfWeek = dayOfWeek;
        this.stateOfDay = stateOfDay;
        HighTemp = highTemp;
        this.lowTemp = lowTemp;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getStateOfDay() {
        return stateOfDay;
    }

    public void setStateOfDay(String stateOfDay) {
        this.stateOfDay = stateOfDay;
    }

    public String getHighTemp() {
        return HighTemp;
    }

    public void setHighTemp(String highTemp) {
        HighTemp = highTemp;
    }

    public String getLowTemp() {
        return lowTemp;
    }

    public void setLowTemp(String lowTemp) {
        this.lowTemp = lowTemp;
    }
}
