package com.sharif.mk.weatherme;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import cz.msebera.android.httpclient.Header;
import com.sharif.mk.weatherme.*;

import java.util.List;

public class MainActivity extends AppCompatActivity {

WeatherModel weatherModel;
ListView listView;
List<WeatherModel> weatherModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();
        AsyncHttpClient client=new AsyncHttpClient();
        client.post(
                "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22tehran%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys"
                , new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {



                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Gson gson=new Gson();
                        GetJsonWeather jsonWeather=gson.fromJson(responseString,GetJsonWeather.class);
                        List<Forecast> forecastList=jsonWeather.getQuery().getResults().getChannel().getItem().getForecast();
                        for (int i=0;i<10;i++){
                            weatherModel.setDayOfWeek(forecastList.get(i).getDay());
                            weatherModel.setStateOfDay(forecastList.get(i).getText());
                            weatherModel.setHighTemp(forecastList.get(i).getHigh());
                            weatherModel.setLowTemp(forecastList.get(i).getLow());

                            weatherModelList.add(weatherModel);


                        }


                    }
                });


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void bind() {

        listView=findViewById(R.id.listview);

        WeatherAdapter weatherAdapter=new WeatherAdapter(this,weatherModelList);
        listView.setAdapter(weatherAdapter);
    }
}
