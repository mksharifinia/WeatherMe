package com.sharif.mk.weatherme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class WeatherAdapter extends BaseAdapter {

    Context mContext;
    List<WeatherModel> weatherModelList;

    public WeatherAdapter(Context mContext, List<WeatherModel> weatherModelList) {
        this.mContext = mContext;
        this.weatherModelList = weatherModelList;
    }

    @Override
    public int getCount() {
        return weatherModelList.size();
    }

    @Override
    public Object getItem(int i) {
        return weatherModelList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row= LayoutInflater.from(mContext).inflate(R.layout.item_list_view,viewGroup,false);
        TextView dayOfWeek=row.findViewById(R.id.day);
        ImageView state=row.findViewById(R.id.state);
        TextView highTemp=row.findViewById(R.id.highT);
        TextView lowTemp=row.findViewById(R.id.LowT);
        dayOfWeek.setText(weatherModelList.get(i).getDayOfWeek());
        setImageStatOfTheDay(state,weatherModelList.get(i).getStateOfDay());
        highTemp.setText(weatherModelList.get(i).getHighTemp());
        lowTemp.setText(weatherModelList.get(i).getLowTemp());


        return row;
    }
    void setImageStatOfTheDay(View view,String state){

        switch (state){
            case "Sunny":
                Glide.with(mContext).load(R.drawable.sunny).into((ImageView) view);
                break;
            case "Cloudy":
                Glide.with(mContext).load(R.drawable.sunny).into((ImageView) view);
                break;
            case "Rainy":
                Glide.with(mContext).load(R.drawable.sunny).into((ImageView) view);
                break;
            default:

        }

    }
}
